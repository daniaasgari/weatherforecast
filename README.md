WEATHERFORECAST

This weather forecast app is based on Angular 1 implementations. 

Running on Windows
In order to run this app locally, you can use “http-server”. Instruction of http-server installation, on Windows, are as follows:
1•	Run “npm install http-server -g” in your command line.
2•	Go the WeatherForecast folder, using your command line, and run “http-server -o”. This will bring up the weather forecast app in your default browser
