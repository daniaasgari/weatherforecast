(function(){

    angular.module('weatherApp')
        .controller('homeController', ['$scope', '$location', '$window', 'weatherService','cityService',
            function ($scope, $location, $window, weatherService,cityService) {
                var self = this;
                $scope.city= cityService.city;
                $scope.errorMessage = null;

                $scope.$watch('city', function(){
                    cityService.city = $scope.city;
                });


                $scope.insertCity = function(path){
                    weatherService.getWeather($scope.city)
                        .then(function (){
                            $scope.city = cityService.city;
                            $scope.weather = weatherService.weather;
                            console.log(path);
                            $window.location.href= path;
                        }, function(error){
                            $scope.errorMessage = 'There is an error. Please try again';
                        });

                }

                $scope.convertToFahr = function (deg){
                    return (Math.round ((1.8 * (deg - 273)) + 32) +" " + "F");
                }

                $scope.convertToCel = function (deg){
                    return (Math.round (deg - 273.15) +" " + "C");
                }

                $scope.convertDate= function(date){
                    return new Date(date * 1000);
                }


            }]);

})();





