(function(){
    
    angular.module('weatherApp')
        .service('weatherService', ['$http', 
    function ($http) {
        var self = this;
        var APIKEY = '8caa3a62ba1f3b52d931888f38d1bc75';
        var apiBase = "http://api.openweathermap.org/data/2.5/forecast/daily" ;
        var days = 5;
        
        self.errorMessage = '';
            
        self.getWeather = function(city){
            return $http.get(apiBase +
                    '?q=' + city + '&cnt=5'+
                    '&APPID=8caa3a62ba1f3b52d931888f38d1bc75')
                    .then(function success(response) {
                            self.weather = response.data.list;

                      });
                     }
        
            }]);
    
    
})();

