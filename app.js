
var weatherApp = angular.module("weatherApp", ['ngRoute']);


weatherApp.config(function ($routeProvider){
    $routeProvider
    .when ('/home', {
        templateUrl: 'views/home.html',
        controller: 'homeController'
        
    })
    .when ('/weather', {
        templateUrl: 'views/weather.html',
        controller: 'weatherController'
        
    })
    .otherwise({
        redirectTo: '/home'
      });
});


